package com.puthisastra.first;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BOOK")
public class Book {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name="B_ID")
	private Long id;
	
	@Column(length=200)
	private String title;
	
	private Integer page;
	
	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTitle(String name) {
		this.title = name;
	}

	public String getTitle() {
		return title;
	}

	
}