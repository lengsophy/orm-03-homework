package com.puthisastra.first;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BookRepository extends JpaRepository<Book, Long> {

	List<Book> findByPageGreaterThan(Integer page);
	List<Book> findByTitle(String title);
	List<Book> findByTitleAndPage(String title, Integer page);
	List<Book> findByPageGreaterThanAndPageLessThan(Integer page1, Integer page2);
	@Query("SELECT coalesce(max(b.page), 0) FROM Book b")
	Integer getMaxPage();
	@Query("Select b from Book as b order by b.title desc")
	List<Book> getBookListByTitle();
	@Query("SELECT b FROM Book as b where b.title = ?1 OR b.page = ?2")
	List<Book> getBookListByTitleOrPage(String title1, Integer page1);
}
