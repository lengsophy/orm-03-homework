package com.puthisastra.first;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class MainController {
	
	@Autowired
	private BookRepository bookRepository;
	

	
	@GetMapping("/create")
    @ResponseBody
    public Book createPerson() {
		Book book = new Book();
		book.setTitle("something");
		book.setPage(99);
		return bookRepository.save(book);
    }
	@GetMapping("/getAllBook")
    @ResponseBody
    public List<Book> getListAdress() {
		return bookRepository.findAll();
    }
	
	@GetMapping("/getBookById")
    @ResponseBody
    public Optional<Book> getBookById() {
		return bookRepository.findById((long) 2);
    }
	
	@GetMapping("/updateBookById")
    @ResponseBody
    public Book updateBookById() {
		Book book = bookRepository.getOne((long) 1);
		book.setTitle("Monster");
		book.setPage(20);
		return bookRepository.save(book);
    }
	@GetMapping("/deleteBookById")
    @ResponseBody
	public void deleteBookById() {
		bookRepository.deleteById((long) 1);
    }
	@GetMapping("/getBookWherePageMoreThan100")
    @ResponseBody
	public List<Book> getBookWherePageMoreThan100() {
		return bookRepository.findByPageGreaterThan(100);
    }
	@GetMapping("/getBookWhereTitle")
    @ResponseBody
	public List<Book> getBookWhereTitle() {
		return bookRepository.findByTitle("something");
    }
	@GetMapping("/getBookWhereTitleAndPageEqueal")
    @ResponseBody
	public List<Book> getBookWhereTitleAndPageEqueal() {
		return bookRepository.findByTitleAndPage("something", 50);
    }
	@GetMapping("/getBookWherePageGreaterThanAndLessThan")
    @ResponseBody
	public List<Book> getBookWherePageGreaterThanAndLessThan() {
		return bookRepository.findByPageGreaterThanAndPageLessThan(50, 100);
    }
	@GetMapping("/getMaxPage")
    @ResponseBody
	public Integer getMaxPage() {
		return bookRepository.getMaxPage();
    }
	@GetMapping("/getBookListByTitle")
    @ResponseBody
	public List<Book> getBookListByTitle() {
		return bookRepository.getBookListByTitle();
    }
	@GetMapping("/getBookListByTitleOrPage")
    @ResponseBody
	public List<Book> getBookListByTitleOrPage() {
		return bookRepository.getBookListByTitleOrPage("something", 50);
    }
}
